package edu.ucsd.gashog;

/**
 * Created by Andrew on 8/22/2015.
 */
public class Car {
    public String make;
    public String model;
    public int modelYear;
    public String state;
    public String engine;

    public Car() {

    }

    public Car(String make, String model, int year, String state, String engine) {
        this.make = make;
        this.model = model;
        this.modelYear = modelYear;
        this.state = state;
        this.engine = engine;
    }

    public String getMake(){
        return this.make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getModelYear() {
        return this.modelYear;
    }

    public void setGals(int modelYear) { this.modelYear = modelYear; }

    public String getState() { return this.state; }

    public void setState(String state) { this.state = state; }

    public String getEngine() { return this.engine; }

    public void setEngine(String engine) { this.engine = engine;}
}
