package edu.ucsd.gashog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Steve on 8/30/2015.
 */
public class AverageItemAdapter extends ArrayAdapter<AverageItem> {
    public AverageItemAdapter(Context context, ArrayList<AverageItem> items) {
        super(context, 0, items);
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent) {
        AverageItem item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.average_item_view_layout, parent, false);
        }

        NumberFormat currencyFormat = DecimalFormat.getCurrencyInstance(Locale.US);
        String formattedAverage = currencyFormat.format(item.average / 100.0);

        TextView tv = (TextView) convertView.findViewById(R.id.average_item_type);
        tv.setText("Octane: " + item.type);

        tv = (TextView) convertView.findViewById(R.id.average_item_average);
        tv.setText("Average: " + formattedAverage);

        return convertView;
    }
}
