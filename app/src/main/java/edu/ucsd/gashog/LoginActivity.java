package edu.ucsd.gashog;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class LoginActivity extends AppCompatActivity {

    private EditText userText;
    private EditText passwordText;
    private Button loginButton;
    private Button registerButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userText = (EditText) findViewById(R.id.user_edit);
        passwordText = (EditText) findViewById(R.id.password_edit);
        loginButton = (Button) findViewById(R.id.login_button);
        registerButton = (Button) findViewById(R.id.register_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginSetup();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerSetup();
            }
        });

        getSupportActionBar().hide();

    }

    public void loginSetup() {
        String username = userText.getText().toString();
        String password = passwordText.getText().toString();

        if(username.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Missing username/password.", Toast.LENGTH_SHORT).show();
        }

        else {
            ParseUser.logInInBackground(username, password, new LogInCallback() {
                @Override
                public void done(ParseUser parseUser, ParseException e) {
                    if(e == null) {
                        Intent MainIntent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(MainIntent);
                    }

                    else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setMessage(e.getMessage()).setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        }
    }

    public void registerSetup() {
        String username = userText.getText().toString();
        String password = passwordText.getText().toString();

        if(username.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Missing email/password.", Toast.LENGTH_SHORT).show();
        }
        else {
            ParseUser newUser = new ParseUser();
            newUser.setEmail(username);
            newUser.setPassword(password);
            newUser.setUsername(username);
            newUser.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if(e == null) {
                        Intent config = new Intent(LoginActivity.this, ConfigurationActivity.class);
                        config.putExtra("callingActivity", 1);
                        startActivity(config);
                    }

                    else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setMessage(e.getMessage()).setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        }
        //ParseUser
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
