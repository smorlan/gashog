package edu.ucsd.gashog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Steve on 8/30/2015.
 */
public class AverageItem {
    public String type;
    public long average;
    public AverageItem(String type, double average) {
        this.type = type;
        this.average = (long)average;
    }
}
