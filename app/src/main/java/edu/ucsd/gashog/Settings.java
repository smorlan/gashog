package edu.ucsd.gashog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


public class Settings extends AppCompatActivity {

    private RelativeLayout _historyBtn;
    private RelativeLayout _mainBtn;
    private RelativeLayout _finBtn;
    private RelativeLayout _addGasbtn;
    private RelativeLayout _settingsBtn;
    private ImageButton _mainBtn2;
    private ImageButton _historyBtn2;
    private ImageButton _finBtn2;
    private ImageButton _addGasbtn2;
    private ImageButton _settingsBtn2;
    private Button editInfoButton;

    private static final int MAIN_ACTIVITY = 1;
    private final static int SETTINGS_ACTIVITY = 2;

    private TextView make, model, year, state, eSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //hide action bar
        getSupportActionBar().hide();



        //create intents that we can navigate to from button clicks
        final Intent MainIntent = new Intent(this, MainActivity.class);
        final Intent HistoryIntent = new Intent(this, History.class);
        final Intent AddGasIntent = new Intent(this, AddGas.class);
        final Intent FinancialsIntent = new Intent(this, Financial.class);
        final Intent config = new Intent(this, ConfigurationActivity.class);

        //set click listeners for top buttons
        _historyBtn = (RelativeLayout) findViewById(R.id.history_settings_button);
        _historyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(HistoryIntent);
            }
        });
        _historyBtn2 = (ImageButton) findViewById(R.id.history_settings_button2);
        _historyBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(HistoryIntent);
            }
        });

        _addGasbtn = (RelativeLayout) findViewById(R.id.fill_gas_settings_button);
        _addGasbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AddGasIntent);
            }
        });
        _addGasbtn2 = (ImageButton) findViewById(R.id.fill_gas_settings_button2);
        _addGasbtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AddGasIntent);
            }
        });

        _finBtn = (RelativeLayout) findViewById(R.id.financial_settings_button);
        _finBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FinancialsIntent);
            }
        });
        _finBtn2 = (ImageButton) findViewById(R.id.financial_settings_button2);
        _finBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FinancialsIntent);
            }
        });

        _mainBtn = (RelativeLayout) findViewById(R.id.home_settings_button);
        _mainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainIntent);
            }
        });
        _mainBtn2 = (ImageButton) findViewById(R.id.home_settings_button2);
        _mainBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainIntent);
            }
        });

        editInfoButton = (Button) findViewById(R.id.edit_info_button);
        editInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.putExtra("callingActivity", SETTINGS_ACTIVITY);
                startActivity(config);
            }
        });

        make = (TextView) findViewById(R.id.vehicle_make);
        model = (TextView) findViewById(R.id.vehicle_model);
        year = (TextView) findViewById(R.id.vehicle_year);
        state = (TextView) findViewById(R.id.vehicle_state);
        eSize = (TextView) findViewById(R.id.vehicle_engine_size);

        extractVehicleInformation();
    }

    public void extractVehicleInformation() {
        SharedPreferences sharedPrefs = getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
        make.setText(sharedPrefs.getString(
                getResources().getString(R.string.vehicle_make), ""
        ));
        model.setText(sharedPrefs.getString(
                getResources().getString(R.string.vehicle_model), ""
        ));
        year.setText(sharedPrefs.getString(
                getResources().getString(R.string.vehicle_year), ""
        ));
        state.setText(sharedPrefs.getString(
                getResources().getString(R.string.state), ""
        ));
        eSize.setText(sharedPrefs.getString(
                getResources().getString(R.string.engine_size), ""
        ));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
