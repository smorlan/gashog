package edu.ucsd.gashog;
/**
 * Created by Steve on 8/17/2015.
 */
public class HistoryListItem {
    public float cost;
    public String gasType;
    public double gallons;
    public int mileage;
    public int month;
    public int year;

    public HistoryListItem() {

    }

    public HistoryListItem(float cost, String gasType, double gallons, int mileage,
                            int month, int year) {
        this.cost = cost;
        this.gasType = gasType;
        this.gallons = gallons;
        this.mileage = mileage;
        this.month = month;
        this.year = year;
    }

    public float getCost(){
        return this.cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getType() {
        return this.gasType;
    }

    public void setType(String gasType) {
        this.gasType = gasType;
    }

    public double getGals() {
        return this.gallons;
    }

    public void setGals(double gallons) { this.gallons = gallons; }

    public void setMileage(int mileage) {this.mileage = mileage; }

    public int getMileage() { return this.mileage; }

    public void setMonth(int month) {this.month = month;}

    public int getMonth() { return this.month; }

    public void setYear(int year) { this.year = year; }

    public int getYear() { return this.year; }
}
