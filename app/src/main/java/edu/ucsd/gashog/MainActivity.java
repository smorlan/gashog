package edu.ucsd.gashog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.Preference;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.util.Log;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private RelativeLayout _historyBtn;
    private RelativeLayout _finBtn;
    private RelativeLayout _addGasbtn;
    private RelativeLayout _settingsBtn;
    private RelativeLayout _homeBtn;

    private ImageButton _historyBtn2;
    private ImageButton _finBtn2;
    private ImageButton _addGasbtn2;
    private ImageButton _settingsBtn2;
    private ImageButton _homeBtn2;

    private Button _logoutBtn;

    private static final int MAIN_ACTIVITY = 1;
    private final static int SETTINGS_ACTIVITY = 2;
    private TextView text;
    private boolean parseInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //db.addEntries();
        // Enable Local Datastore.

        // Test
        /*ParseObject testObject = new ParseObject("TestObject");
        testObject.put("foo", "bar");
        testObject.saveInBackground();*/


        //Check shared preferences for previous configuration
        ParseUser currentUser = ParseUser.getCurrentUser();
        if(currentUser == null) {
            Intent login = new Intent(this, LoginActivity.class);
            startActivity(login);
        }
        else {
            HistoryDatabaseHelper db = new HistoryDatabaseHelper(this);
            db.getWritableDatabase();
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Car");
            query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());
            query.orderByDescending("updatedAt");
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    SharedPreferences.Editor editor = getSharedPreferences("SETTINGS", Context.MODE_PRIVATE).edit();
                    editor.putString(getResources().getString(R.string.vehicle_make), parseObject.get("make").toString());
                    editor.putString(getResources().getString(R.string.vehicle_model), parseObject.get("model").toString());
                    editor.putString(getResources().getString(R.string.vehicle_year), parseObject.get("year").toString());
                    editor.putString(getResources().getString(R.string.engine_size), parseObject.get("engine").toString());
                    editor.putString(getResources().getString(R.string.state), parseObject.get("state").toString());
                    editor.putBoolean(getResources().getString(R.string.user_configured), true);
                    editor.commit();
                }
            });
            populateData();
        }

        /*SharedPreferences sharedPref = this.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);



        boolean userConfigured = sharedPref.getBoolean(
                getResources().getString(R.string.user_configured), false
        );
        if (!userConfigured) {
            //redirect to editing activity
            Intent config = new Intent(this, ConfigurationActivity.class);
            config.putExtra("callingActivity", MAIN_ACTIVITY);
            startActivity(config);*/

        //hide action bar (THE TOP MENU BAR DUMBASS)
        getSupportActionBar().hide();

        //create intents that we can navigate to from button clicks
        final Intent HistoryIntent = new Intent(this, History.class);
        final Intent AddGasIntent = new Intent(this, AddGas.class);
        final Intent FinancialsIntent = new Intent(this, Financial.class);
        final Intent SettingsIntent = new Intent(this, Settings.class);
        final Intent LoginIntent = new Intent(this, LoginActivity.class);


        //set click listeners for top buttons
        _historyBtn = (RelativeLayout) findViewById(R.id.history_main_button);
        _historyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(HistoryIntent);
            }
        });
        _historyBtn2 = (ImageButton) findViewById(R.id.history_main_button2);
        _historyBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(HistoryIntent);
            }
        });

        _addGasbtn = (RelativeLayout) findViewById(R.id.fill_gas_main_button);
        _addGasbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AddGasIntent);
            }
        });
        _addGasbtn2 = (ImageButton) findViewById(R.id.fill_gas_main_button2);
        _addGasbtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AddGasIntent);
            }
        });

        _finBtn2 = (ImageButton) findViewById(R.id.financial_main_button2);
        _finBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FinancialsIntent);
            }
        });
        _finBtn = (RelativeLayout) findViewById(R.id.financial_main_button);
        _finBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FinancialsIntent);
            }
        });


        _settingsBtn = (RelativeLayout) findViewById(R.id.settings_main_button);
        _settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingsIntent);
            }
        });
        _settingsBtn2 = (ImageButton) findViewById(R.id.settings_main_button2);
        _settingsBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingsIntent);
            }
        });

        _homeBtn = (RelativeLayout) findViewById(R.id.home_main_button);
        _homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //already home, do nothing
            }
        });
        _homeBtn2 = (ImageButton) findViewById(R.id.home_main_button2);
        _homeBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //already home, do nothing
            }
        });

        _logoutBtn = (Button) findViewById(R.id.logout_button);
        _logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                startActivity(LoginIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void logout() {
        HistoryDatabaseHelper db = new HistoryDatabaseHelper(this);
        db.clearDB();
        ParseUser.logOut();
        Toast.makeText(this, "Logout successful", Toast.LENGTH_SHORT).show();
    }

    public void clearData()     {
        text = (TextView) findViewById(R.id.octane_header);
        text.setText("");
    }


    public void populateData()   {
        HistoryDatabaseHelper db = new HistoryDatabaseHelper(this);
        text = (TextView) findViewById(R.id.last_fill_header);
        //TextView lastData = (TextView) findViewById(R.id.last_fill_data);
        if(db.getAllEntries().isEmpty())    {
            text.setText("You have yet to make your first fill up.");
            clearData();
            return;
        }



        HistoryListItem item = db.getAllEntries().get(db.getAllEntries().size()-1);
        SpannableString content = new SpannableString("Your Last Fill Up");
        text.setText(content);

        TextView tv = (TextView) findViewById(R.id.main_metrics_date);
        tv.setText("Date: " + item.getMonth() + "/" + item.getYear());

        tv = (TextView) findViewById(R.id.main_metrics_cost);
        tv.setText("Cost: " + item.getCost());

        tv = (TextView) findViewById(R.id.main_metrics_gallons);
        tv.setText("Gallons: "+item.getGals());

        tv = (TextView) findViewById(R.id.main_metrics_type);
        tv.setText("Octane: "+item.getType());

        text = (TextView) findViewById(R.id.octane_header);

        SpannableString listHeader = new SpannableString("Use Of Octane\nAverage COST/GAL");
        text.setText(listHeader);
        double result;
        ArrayList<AverageItem> items = new ArrayList<AverageItem>();
        String list = "";
        ListView listView = (ListView) findViewById(R.id.list_view_averages_main);

        result = db.costByType("85")/db.totalByType("85");
        if(result > 0.00) {
            list += "85: $" + result / 100 + " / " + db.avgFillByType("85");
            items.add(new AverageItem("85", result));
        }

        result = db.costByType("87")/db.totalByType("87");
        if(result > 0.00) {
            list += "\n87: $" + result / 100 + " / " + db.avgFillByType("87");
            items.add(new AverageItem("87", result));
        }

        result = db.costByType("89")/db.totalByType("89");
        if(result > 0.00) {
            list += "\n89: $" + result / 100 + " / " + db.avgFillByType("89");
            items.add(new AverageItem("89", result));
        }

        result = db.costByType("91")/db.totalByType("91");
        if(result > 0.00) {
            list += "\n91: $" + result / 100 + " / " + db.avgFillByType("91");
            items.add(new AverageItem("91", result));
        }

        AverageItemAdapter adapter = new AverageItemAdapter(this, items);
        listView.setAdapter(adapter);

        /*
        result = db.avgByType("Diesel");
        if(result != 0.0)
            list += "\nDiesel: " + result;


        result = db.avgByType("Ethanol");
        if(result != 0.0)
            list += "\nEthanol: " + result;


        text.setText(list);

    */

    }
}