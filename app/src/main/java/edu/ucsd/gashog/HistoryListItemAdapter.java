package edu.ucsd.gashog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Steve on 8/17/2015.
 */
public class HistoryListItemAdapter extends ArrayAdapter<HistoryListItem> {
    public HistoryListItemAdapter(Context context, ArrayList<HistoryListItem> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HistoryListItem item = getItem(position);

        //if android fucks itself
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.history_list_view_layout, parent, false);
        }
        //put data into layout item
        NumberFormat currencyFormat = DecimalFormat.getCurrencyInstance(Locale.US);
        String formattedText = currencyFormat.format(item.cost / 100.0);
        TextView costTextView = (TextView) convertView.findViewById(R.id.cost);
        costTextView.setText(formattedText);

        //gas type
        TextView gasTextView = (TextView) convertView.findViewById(R.id.gas_type);
        gasTextView.setText(item.gasType);

        //gallons
        NumberFormat gallonsFormat = NumberFormat.getNumberInstance();
        gallonsFormat.setMaximumFractionDigits(2);
        gallonsFormat.setMinimumFractionDigits(2);
        String formattedGallons = gallonsFormat.format(item.gallons);
        TextView gallonsTextView = (TextView) convertView.findViewById(R.id.gallons);
        gallonsTextView.setText(formattedGallons + "g");

        return convertView;
        }
    }
