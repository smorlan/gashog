package edu.ucsd.gashog;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by Andrew on 8/18/2015.
 */
public class HistoryDatabaseHelper extends SQLiteOpenHelper {

    //DB info
    private static final String DB_Name = "GasHogDatabase";
    private static final String histTable = "history";
    private static final String vecTable = "vehicles";
    private static int DB_Version = 1;
    private static final String FILL_ID = "id";
    private static final String COST = "cost";
    private static final String TYPE = "gasType";
    private static final String GALLONS = "gallons";
    private static final String MILEAGE = "mileage";
    private static final String MONTH = "month";
    private static final String YEAR = "year";
    private static final String CAR_NUM = "carNum";
    private static final String MAKE = "make";
    private static final String MODEL = "model";
    private static final String MODEL_YEAR = "modelYear";
    private static final String STATE = "state";
    private static final String ENGINE = "engine";



    /*private static HistoryDatabaseHelper sInstance;

    public static synchronized HistoryDatabaseHelper getInstance(Context context) {
        if(sInstance == null) {
            sInstance = new HistoryDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }*/

    public HistoryDatabaseHelper(Context context) {
        super(context, DB_Name, null, DB_Version);
    }

    @Override
    public void onCreate(final SQLiteDatabase db){
        String CREATE_HISTORY_TABLE = "CREATE TABLE " + histTable + "(" + FILL_ID + " INTEGER PRIMARY KEY," + COST + " FLOAT,"
                + TYPE + " TEXT," + GALLONS + " INT," + MILEAGE + " INT," + MONTH + " INT,"
                + YEAR + " INT" + ")";
        String CREATE_VEHICLE_TABLE = "CREATE TABLE " + vecTable + "(" + CAR_NUM + " INTEGER PRIMARY KEY," +
               MAKE + " TEXT," + MODEL + " TEXT," + MODEL_YEAR + " INT," + STATE + " TEXT," + ENGINE + " TEXT)";
        db.execSQL(CREATE_HISTORY_TABLE);
        db.execSQL(CREATE_VEHICLE_TABLE);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("HistoryItem");
        query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> historyList, ParseException e) {
                if (e == null) {
                    for (ParseObject gasObjects : historyList) {
                        db.execSQL("INSERT INTO history (cost, gasType, gallons, mileage, month, year) VALUES (" +
                                gasObjects.get("cost") + ", " + gasObjects.get("gasType") + ", " + gasObjects.get("gallons")
                                + ", " + gasObjects.get("miles") + ", " + gasObjects.get("month") + ", " + gasObjects.get("year")
                                + ")");
                    }
                }

            }
        });

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
        if(oldV != newV) {
            db.execSQL("DROP TABLE IF EXISTS " + histTable);
            db.execSQL("DROP TABLE IF EXISTS " + vecTable);
            onCreate(db);
        }
    }

    public void clearDB() {
        DB_Version++;
    }

    //GAS HISTORY METHODS
    public void addEntries() {
        SQLiteDatabase db = this.getWritableDatabase();
        String ADD_ENTRY1 = "Insert into history (cost, gasType, gallons, mileage, month, year) VALUES('3266', '87', 55, 1, 5, 2013)";
        db.execSQL(ADD_ENTRY1);
        String ADD_ENTRY2 = "Insert into history (cost, gasType, gallons, mileage, month, year) VALUES('4357', '91', 35, 1, 8, 2015)";
        db.execSQL(ADD_ENTRY2);
        String ADD_ENTRY3 = "Insert into history (cost, gasType, gallons, mileage, month, year) VALUES('8899', '87', 55, 1, 6, 2013)";
        db.execSQL(ADD_ENTRY3);
        String ADD_ENTRY4 = "Insert into history(cost, gasType, gallons, mileage, month, year) VALUES('2960', '91', 55, 1, 7, 2014)";
        db.execSQL(ADD_ENTRY4);
        String ADD_ENTRY5 = "Insert into history(cost, gasType, gallons, mileage, month, year) VALUES('4258', '87', 55, 1, 9, 2013)";
        db.execSQL(ADD_ENTRY5);
        String ADD_ENTRY6 = "Insert into history(cost, gasType, gallons, mileage, month, year) VALUES('7688', '85', 55, 1, 2, 2014)";
        db.execSQL(ADD_ENTRY6);
        String ADD_ENTRY7 = "Insert into history(cost, gasType, gallons, mileage, month, year) VALUES('9906', '89', 88, 1, 3, 2015)";
        db.execSQL(ADD_ENTRY7);
        db.close();
    }
    
    public void addEntry(HistoryListItem entry) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COST, entry.getCost());
        values.put(TYPE, entry.getType());
        values.put(GALLONS, entry.getGals());
        values.put(MILEAGE, entry.getMileage());
        values.put(MONTH, entry.getMonth());
        values.put(YEAR, entry.getYear());

        db.insert(histTable, null, values);
        db.close();
    }

    public HistoryListItem getEntry(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(histTable, new String[]{FILL_ID,
                        COST, TYPE, GALLONS}, FILL_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if(cursor != null)
            cursor.moveToFirst();

        HistoryListItem entry = new HistoryListItem(cursor.getFloat(1), cursor.getString(2), cursor.getInt(3),
                cursor.getInt(4), cursor.getInt(5), cursor.getInt(6));
        return entry;
    }

    public List<HistoryListItem> getAllEntries() {
        List<HistoryListItem> historyList = new ArrayList<HistoryListItem>();

        String selectQuery = "SELECT * FROM " + histTable;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                HistoryListItem entry = new HistoryListItem();
                entry.setCost(cursor.getFloat(1));
                entry.setType(cursor.getString(2));
                entry.setGals(cursor.getFloat(3));
                entry.setMileage(cursor.getInt(4));
                entry.setMonth(cursor.getInt(5));
                entry.setYear(cursor.getInt(6));

                historyList.add(entry);
            } while(cursor.moveToNext());
        }
        cursor.close();

        return historyList;
    }

    public List<HistoryListItem> getMonthlyEntries() {
        List<HistoryListItem> historyList = new ArrayList<HistoryListItem>();

        String selectQuery = "SELECT AVG(cost), AVG(gallons), AVG(mileage), month, year FROM history GROUP BY month";
        String gasQuery = "SELECT gasType, month FROM history GROUP BY gasType, month ORDER BY COUNT(gasType) DESC LIMIT 1;";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Cursor gasCursor = db.rawQuery(gasQuery, null);

        if(cursor.moveToFirst()) {
            if(gasCursor.moveToFirst()) {
                do {
                    HistoryListItem entry = new HistoryListItem();
                    entry.setCost(cursor.getFloat(0));
                    entry.setType(gasCursor.getString(0));
                    entry.setGals(cursor.getFloat(1));
                    entry.setMileage(cursor.getInt(2));
                    entry.setMonth(cursor.getInt(3));
                    entry.setYear(cursor.getInt(4));

                    historyList.add(entry);
                } while (cursor.moveToNext() && gasCursor.moveToNext());
            }
        }
        cursor.close();

        return historyList;
    }

    public List<HistoryListItem> getYearlyEntries() {
        List<HistoryListItem> historyList = new ArrayList<HistoryListItem>();

        String selectQuery = "SELECT AVG(cost), AVG(gallons), AVG(mileage), month, year FROM history GROUP BY year";
        String gasQuery = "SELECT gasType, year FROM history GROUP BY gasType, year ORDER BY COUNT(gasType) DESC LIMIT 1;";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Cursor gasCursor = db.rawQuery(gasQuery, null);

        if(cursor.moveToFirst()) {
            if(gasCursor.moveToFirst()) {
                do {
                    HistoryListItem entry = new HistoryListItem();
                    entry.setCost(cursor.getFloat(0));
                    entry.setType(gasCursor.getString(0));
                    entry.setGals(cursor.getFloat(1));
                    entry.setMileage(cursor.getInt(2));
                    entry.setMonth(cursor.getInt(3));
                    entry.setYear(cursor.getInt(4));

                    historyList.add(entry);
                } while (cursor.moveToNext() && gasCursor.moveToNext());
            }
        }
        cursor.close();

        return historyList;
    }

    public int getMaxDistance() {
        int maxDist = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor test = db.rawQuery("SELECT COUNT(*) FROM " + histTable, null);
        test.moveToFirst();
        int isEmpty = test.getInt(0);
        if(isEmpty > 0) {
            Cursor cursor = db.rawQuery("SELECT MAX(" + MILEAGE + ") FROM " + histTable, null);
            if (cursor.moveToFirst()) {
                maxDist = cursor.getInt(0);
            }
        }
        return maxDist;
    }

    public float avgCost() {
        float avgCost = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT AVG(" + COST + ") FROM " + histTable, null);
        if(cursor.moveToFirst()) {
            avgCost = cursor.getFloat(0);
        }

        return avgCost;
    }

    public float totalCost() {
        float totalCost = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(" + COST + ") FROM " + histTable, null);
        if(cursor.moveToFirst()) {
            totalCost = cursor.getFloat(0);
        }

        return totalCost;
    }

    public float thisYearCost(int year) {
        float thisYear = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(" + COST + ") FROM " + histTable + " WHERE " + YEAR + " = " + year, null);
        if(cursor.moveToFirst()) {
            thisYear = cursor.getFloat(0);
        }

        return thisYear;
    }

    public List<Float> yearlyCost() {
        float yearlyCost = 0;
        List<Float> yearlyList = new ArrayList<Float>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(" + MILEAGE + ") FROM " + histTable + " GROUP BY " + YEAR, null);
        if(cursor.moveToFirst()) if(cursor.moveToFirst()) {
            do {
                yearlyCost = cursor.getInt(0);
                yearlyList.add(yearlyCost);
            } while(cursor.moveToNext());
        }

        return yearlyList;
    }

    public float thisMonthCost(int month) {
        float thisMonth = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(" + COST + ") FROM " + histTable + " WHERE " + MONTH + " = " + month, null);
        if(cursor.moveToFirst()) {
            thisMonth = cursor.getFloat(0);
        }

        return thisMonth;
    }

    public List<Float> monthlyCost() {
        float monthlyCost = 0;
        List<Float> monthlyList = new ArrayList<Float>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(" + COST + ") FROM " + histTable + " GROUP BY " + MONTH, null);
        if(cursor.moveToFirst()) {
            do {
                monthlyCost = cursor.getInt(0);
                monthlyList.add(monthlyCost);
            }while(cursor.moveToNext());
        }

        return monthlyList;
    }

    public float avgGallons() {
        float avgGallons = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT AVG(" + GALLONS + ") FROM " + histTable, null);
        if(cursor.moveToFirst()) {
            avgGallons = cursor.getFloat(0);
        }

        return avgGallons;
    }

    public double lastFill() {
        double lastFill = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + GALLONS + " FROM " + histTable, null);
        if(cursor.moveToFirst()) {
            cursor.moveToLast();
            lastFill = cursor.getFloat(0);
        }

        return lastFill;
    }

    public double lastFillCost()    {
        double lastFill = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + COST + " FROM " + histTable, null);
        if(cursor.moveToFirst()) {
            cursor.moveToLast();
            lastFill = cursor.getFloat(0);
        }

        return lastFill;
    }

    public double avgFillByType(String type) {
        double fillByType = 0;
        List<Double> typeList = new ArrayList<Double>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT AVG(" + GALLONS + ") FROM " + histTable + " WHERE " + TYPE + " = " + type, null);
        if(cursor.moveToFirst()) {
            fillByType = cursor.getInt(0);
        }

        return fillByType;
    }

    public double totalByType(String type) {
        double totalByType = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(" + GALLONS + ") FROM " + histTable + " WHERE " + TYPE + " = " + type, null);
        if(cursor.moveToFirst()) {
            totalByType = cursor.getInt(0);
        }

        return totalByType;
    }

    public float avgByType(String type) {
        float avgPerType = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT AVG(" + COST + ") FROM " + histTable + " WHERE " + TYPE + " = " + type, null);
        if(cursor.moveToFirst()) {
            avgPerType = cursor.getInt(0);
        }

        return avgPerType;
    }

    public float costByType(String type) {
        float totalPerType = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(" + COST + ") FROM " + histTable + " WHERE " + TYPE + " = " + type, null);
        if(cursor.moveToFirst()) {
            totalPerType = cursor.getInt(0);
        }

        return totalPerType;
    }

    //CAR DATA METHODS
    public void addCar(Car car) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MAKE, car.getMake());
        values.put(MODEL, car.getModel());
        values.put(MODEL_YEAR, car.getModelYear());
        values.put(STATE, car.getState());
        values.put(ENGINE, car.getEngine());

        db.insert(vecTable, null, values);

        //Log.i(LOG_TAG, "Database Opened");
        db.close();
    }

    Car getCar(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(vecTable, new String[]{CAR_NUM,
                        MAKE, MODEL, MODEL_YEAR, STATE, ENGINE}, CAR_NUM + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if(cursor != null)
            cursor.moveToFirst();

        Car car = new Car(cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4), cursor.getString(5));
        return car;
    }


}
