package edu.ucsd.gashog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseUser;

public class ConfigurationActivity extends AppCompatActivity {

    private Button submitBtn;
    private EditText make, model, year, eSize, state;
    private int callingActivity;
    private static final int MAIN_ACTIVITY = 1;
    private final static int SETTINGS_ACTIVITY = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        getSupportActionBar().hide();

        //get name of calling activity
        callingActivity = 0;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                callingActivity = 0;
            } else {
                callingActivity = extras.getInt("callingActivity");
            }
        } else {
            callingActivity = (int) savedInstanceState.getSerializable("callingActivity");
        }

        submitBtn = (Button) findViewById(R.id.config_submit_button);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(make.getText().toString().isEmpty() || model.getText().toString().isEmpty() || year.getText().toString().isEmpty()
                        || state.getText().toString().isEmpty() || eSize.getText().toString().isEmpty()){
                    Toast.makeText(ConfigurationActivity.this, "All fields are required", Toast.LENGTH_SHORT).show();
                }
                else {
                    handleSubmit();
                }
            }
        });

        make = (EditText) findViewById(R.id.config_vehicle_make);
        model = (EditText) findViewById(R.id.config_vehicle_model);
        year = (EditText) findViewById(R.id.config_vehicle_year);
        eSize = (EditText) findViewById(R.id.config_vehicle_engine_size);
        state = (EditText) findViewById(R.id.config_vehicle_state);
    }

    public void handleSubmit() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        ParseObject carObject = new ParseObject("Car");
        carObject.put("make", make.getText().toString());
        carObject.put("model", model.getText().toString());
        carObject.put("year", year.getText().toString());
        carObject.put("state", state.getText().toString());
        carObject.put("engine", eSize.getText().toString());
        carObject.put("username", currentUser.getUsername());
        carObject.saveInBackground();
        SharedPreferences.Editor editor = getSharedPreferences("SETTINGS", Context.MODE_PRIVATE).edit();
        editor.putString(getResources().getString(R.string.vehicle_make), make.getText().toString());
        editor.putString(getResources().getString(R.string.vehicle_model), model.getText().toString());
        editor.putString(getResources().getString(R.string.vehicle_year), year.getText().toString());
        editor.putString(getResources().getString(R.string.engine_size), eSize.getText().toString());
        editor.putString(getResources().getString(R.string.state), state.getText().toString());
        editor.putBoolean(getResources().getString(R.string.user_configured), true);
        editor.commit();
        Toast.makeText(this, "Vehicle settings stored successfully!", Toast.LENGTH_SHORT);
        //redirect back to MainActivity
        redirectBackToCaller();
    }

    public void redirectBackToCaller() {
        Intent i;
        switch (callingActivity) {
            case MAIN_ACTIVITY:
                i = new Intent(this, MainActivity.class);
                startActivity(i);
                finish();
                break;
            case SETTINGS_ACTIVITY:
                i = new Intent(this, Settings.class);
                startActivity(i);
                finish();
                break;
            default: break;
        }
    }

    public boolean isEmpty(EditText t) {
        if (t.getText().toString().length() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_configuration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
