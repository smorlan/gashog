package edu.ucsd.gashog;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.StringTokenizer;


public class AddGas extends AppCompatActivity implements ConnectionCallbacks,
        OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private ArrayList<String> gas_stations = new ArrayList<String>();
    private double mLatitude = 0.0;
    private double mLongitude = 0.0;
    private Calendar cal;
    private int month;
    private int year;

    private RelativeLayout _historyBtn;
    private RelativeLayout _finBtn;
    private RelativeLayout _settingsBtn;
    private RelativeLayout _homeBtn;
    private ImageButton _historyBtn2;
    private ImageButton _finBtn2;
    private ImageButton _settingsBtn2;
    private ImageButton _homeBtn2;

    private boolean connection = false;

    private Button _type85;
    private Button _type87;
    private Button _type89;
    private Button _type91;
    private Button _typeDiesel;
    private Button _typeEthanol;
    private Button _focusedBtn;
    private Button _previousBtn;
    private Button _entryButton;
    private String gasType = "";
    private Button mButton;
    private EditText costEdit;
    private float cost;
    private EditText distanceEdit;
    private int distance;
    private int maxDist;
    private EditText gallonsEdit;
    private double gallons;
    private String gas;
    private TextView currentStation;
    private String curStation ="";
    private HistoryDatabaseHelper db = new HistoryDatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_gas);

        //hide action bar
        getSupportActionBar().hide();

        //build Google Services API client
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        currentStation = (TextView) findViewById(R.id.current_station);

        //create intents that we can navigate to from button clicks
        final Intent MainIntent = new Intent(this, MainActivity.class);
        final Intent HistoryIntent = new Intent(this, History.class);
        final Intent FinancialsIntent = new Intent(this, Financial.class);
        final Intent SettingsIntent = new Intent(this, Settings.class);

        //set click listeners for top buttons
        //set click listeners for top buttons
        _historyBtn = (RelativeLayout) findViewById(R.id.history_add_gas_button);
        _historyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(HistoryIntent);
            }
        });
        _historyBtn2 = (ImageButton) findViewById(R.id.history_add_gas_button2);
        _historyBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(HistoryIntent);
            }
        });


        _finBtn2 = (ImageButton) findViewById(R.id.financial_add_gas_button2);
        _finBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FinancialsIntent);
            }
        });
        _finBtn = (RelativeLayout) findViewById(R.id.financial_add_gas_button);
        _finBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FinancialsIntent);
            }
        });


        _settingsBtn = (RelativeLayout) findViewById(R.id.settings_add_gas_button);
        _settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingsIntent);
            }
        });
        _settingsBtn2 = (ImageButton) findViewById(R.id.settings_add_gas_button2);
        _settingsBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingsIntent);
            }
        });

        _homeBtn = (RelativeLayout) findViewById(R.id.home_add_gas_button);
        _homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainIntent);

            }
        });
        _homeBtn2 = (ImageButton) findViewById(R.id.home_add_gas_button2);
        _homeBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainIntent);
            }
        });

        cal = Calendar.getInstance();
        setUpButtons();
    }

    public void setUpButtons() {

        _type85 = (Button) findViewById(R.id.gas_type_85);
        _type87 = (Button) findViewById(R.id.gas_type_87);
        _type89 = (Button) findViewById(R.id.gas_type_89);
        _type91 = (Button) findViewById(R.id.gas_type_91);
        _typeDiesel = (Button) findViewById(R.id.gas_type_diesel);
        _typeEthanol = (Button) findViewById(R.id.gas_type_ethanol);
        _entryButton = (Button) findViewById(R.id.add_entry);

        _type85.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleButtonClick(v);
            }
        });

        _type87.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleButtonClick(v);
            }
        });

        _type89.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleButtonClick(v);
            }
        });

        _type91.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleButtonClick(v);
            }
        });

        _typeDiesel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleButtonClick(v);
            }
        });

        _typeEthanol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleButtonClick(v);
            }
        });

        _entryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleButtonClick(v);
            }
        });

        costEdit = (EditText)findViewById(R.id.price_add_gas_edit_text);
        costEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            private String current = "";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    costEdit.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,.]", "");

                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance().format((parsed/100));

                    current = formatted;
                    costEdit.setText(formatted);
                    costEdit.setSelection(formatted.length());

                    costEdit.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        gallonsEdit = (EditText) findViewById(R.id.gallons_add_gas_edit_text);

        distanceEdit = (EditText) findViewById(R.id.mileage_add_gas_edit_text);


        _focusedBtn = _type85;
        _previousBtn = _type85;
        gas = "85";
    }

    public void handleButtonClick(View view) {
        int id = view.getId();
        switch(id) {
            case R.id.gas_type_85:
                Log.v("Click", "85");
                _previousBtn = _focusedBtn;
                _focusedBtn = _type85;
                gas = "85";
                updateFocused();
                break;
            case R.id.gas_type_87:
                Log.v("Click", "87");
                _previousBtn = _focusedBtn;
                _focusedBtn = _type87;
                gas = "87";
                updateFocused();
                break;
            case R.id.gas_type_89:
                Log.v("Click", "89");
                _previousBtn = _focusedBtn;
                _focusedBtn = _type89;
                gas = "89";
                updateFocused();
                break;
            case R.id.gas_type_91:
                _previousBtn = _focusedBtn;
                _focusedBtn = _type91;
                gas = "91";
                updateFocused();
                break;
            case R.id.gas_type_diesel:
                _previousBtn = _focusedBtn;
                _focusedBtn = _typeDiesel;
                gas = "Diesel";
                updateFocused();
                break;
            case R.id.gas_type_ethanol:
                _previousBtn = _focusedBtn;
                _focusedBtn = _typeEthanol;
                gas = "Ethanol";
                updateFocused();
                break;
            case R.id.add_entry:
                if (isEmpty(gallonsEdit) || isEmpty(distanceEdit) || isEmpty(costEdit)) {
                    Toast.makeText(this, "All fiedls required", Toast.LENGTH_SHORT).show();
                } else {
                    gallons = Double.parseDouble(gallonsEdit.getText().toString());
                    distance = Integer.parseInt(distanceEdit.getText().toString());
                    String formattedText = costEdit.getText().toString();
                    NumberFormat format = DecimalFormat.getCurrencyInstance(Locale.US);
                    month = cal.get(Calendar.MONTH);
                    year = cal.get(Calendar.YEAR);
                    try {
                        Number num = format.parse(formattedText);
                        cost = (num.floatValue() * 100);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    maxDist = db.getMaxDistance();
                    if (distance <= maxDist) {
                        Toast.makeText(this, "Distance entered is less than current total.", Toast.LENGTH_SHORT).show();
                    } else {
                        newGasEntry();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                }
                break;
            default:
                break;
        }
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public void updateFocused() {
        _previousBtn.setBackgroundResource(R.drawable.button_not_focused);
        _previousBtn.setTextColor(Color.rgb(0, 0, 0));
        _focusedBtn.setBackgroundResource(R.drawable.button_focused);
        _focusedBtn.setTextColor(Color.rgb(255, 255, 255));
    }

    public void newGasEntry() {
        db.addEntry(new HistoryListItem(cost, gas, gallons, distance, month, year));
        db.close();
        Toast.makeText(this, "Entry added succesfully", Toast.LENGTH_SHORT).show();
        ParseUser currentUser = ParseUser.getCurrentUser();
        ParseObject gasObject = new ParseObject("HistoryItem");
        gasObject.put("cost", cost);
        gasObject.put("gasType", gas);
        gasObject.put("gallons",gallons);
        gasObject.put("miles", distance);
        //gasObject.put("station", curStation);
        gasObject.put("month", month);
        gasObject.put("year", year);
        gasObject.put("username", currentUser.getUsername());
        gasObject.saveInBackground();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_gas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    StringTokenizer st;

    @Override
    public void onConnected(Bundle bundle) {
        Log.i("CON","Connection Established.");

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            mLatitude = (mLastLocation.getLatitude());
            mLongitude = (mLastLocation.getLongitude());
            Log.i("CON","Latitude: " + mLatitude);
            Log.i("CON","Longitude: " + mLongitude);
        }
        else
            Log.i("CON","Location was Null");

        String address = "https://maps.googleapis.com/" +
                "maps/api/place/nearbysearch/" +
                "json?location="+mLatitude+","+mLongitude +
                "&rankby=distance" +
                "&types=gas_station" +
                "&key=AIzaSyCZH8WOwO_-nE-hCg6JXIUc8Rphm8t2ygY";
        new GasStationSearch().execute(address);
    }

    @Override
    public void onConnectionSuspended(int i) {
        connection = false;
        Log.i("CON","Connection Suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        connection = false;
        Log.i("CON","Connection Failed");
    }

    public class GasStationSearch extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection http;
            try {
                Log.i("CON",params[0]);
                URL url = new URL(params[0]);
                Log.i("CON", "Got URL");
                http = (HttpURLConnection)url.openConnection();
                http.setReadTimeout(2 * 1000);
                http.setConnectTimeout(2 * 1000);
                Log.i("CON","Opened Connection to URL");
                InputStream is = http.getInputStream();
                Log.i("CON","Got Input Stream");
                java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
                String data = s.next();
                Log.i("CON","Converted to String");
                st = new StringTokenizer(data,"\"");

                for(int i = 0; i < 5; i++) {
                    while ((st.hasMoreTokens()) && (!st.nextToken().equals("name"))) {
                    }

                    if (st.hasMoreTokens()) {
                        st.nextToken();
                        String name = st.nextToken();
                        Log.i("CON", name);
                        gas_stations.add(name);
                    }
                }
                http.disconnect();
            }
            catch(Exception e)  {
                e.printStackTrace();
                Log.i("CON",e.toString());
                Log.i("CON","Try Catch Failed",e);
            }

            if(!gas_stations.isEmpty())
                curStation = gas_stations.get(0);
            return curStation;
        }

        @Override
        protected void onPostExecute(String result) {
            if(!curStation.equals("")) {
                TextView txt = (TextView) findViewById(R.id.current_station);
                txt.setText("Filling Up At: " + result);
            }

        }
    }
}