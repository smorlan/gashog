package edu.ucsd.gashog;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.Parse;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Financial extends AppCompatActivity {

    private RelativeLayout _mainBtn;
    private RelativeLayout _historyBtn;
    private RelativeLayout _addGasbtn;
    private RelativeLayout _settingsBtn;

    private ImageButton _mainBtn2;
    private ImageButton _historyBtn2;
    private ImageButton _addGasbtn2;
    private ImageButton _settingsBtn2;

    private TextView lastFillUp;
    private TextView averageFillUp;
    private TextView totalThisYear;
    private TextView totalThisMonth;
    private Calendar cal;
    private HistoryDatabaseHelper db = new HistoryDatabaseHelper(this);



    private ArrayList<HistoryListItem> listItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_financial);



        //hide action bar
        getSupportActionBar().hide();

        //Set Text Views
        lastFillUp = (TextView) findViewById(R.id.last_fill_up);
        averageFillUp = (TextView) findViewById(R.id.average_fill_up);
        totalThisYear = (TextView) findViewById(R.id.this_year_total);
        totalThisMonth = (TextView) findViewById(R.id.this_month_total);

        //create intents that we can navigate to from button clicks
        final Intent HistoryIntent = new Intent(this, History.class);
        final Intent AddGasIntent = new Intent(this, AddGas.class);
        final Intent SettingsIntent = new Intent(this, Settings.class);
        final Intent MainIntent = new Intent(this, MainActivity.class);

        //set click listeners for top buttons
        _historyBtn = (RelativeLayout) findViewById(R.id.history_financial_button);
        _historyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(HistoryIntent);
            }
        });
        _historyBtn2 = (ImageButton) findViewById(R.id.history_financial_button2);
        _historyBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(HistoryIntent);
            }
        });
        _mainBtn = (RelativeLayout) findViewById(R.id.home_financial_button);
        _mainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainIntent);
            }
        });
        _mainBtn2 = (ImageButton) findViewById(R.id.home_financial_button2);
        _mainBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainIntent);
            }
        });
        _addGasbtn = (RelativeLayout) findViewById(R.id.fill_gas_financial_button);
        _addGasbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AddGasIntent);
            }
        });

        _addGasbtn2 = (ImageButton) findViewById(R.id.fill_gas_financial_button2);
        _addGasbtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AddGasIntent);
            }
        });

        _settingsBtn = (RelativeLayout) findViewById(R.id.settings_financial_button);
        _settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingsIntent);
            }
        });
        _settingsBtn2 = (ImageButton) findViewById(R.id.settings_financial_button2);
        _settingsBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingsIntent);
            }
        });

        cal = Calendar.getInstance();
        populateFinancialData();
    }

    private String temp;
    private float sum = 0;

    private void populateFinancialData()    {

        listItems = new ArrayList<HistoryListItem>();
        HistoryDatabaseHelper db = new HistoryDatabaseHelper(this);
        List<HistoryListItem> entries = db.getAllEntries();

        for(int i = 0; i < entries.size(); i++)
            listItems.add(entries.get(i));

        if(listItems.isEmpty()) {
            lastFillUp.setText("$0.00");
            averageFillUp.setText("$0.00");
            totalThisYear.setText("$0.00");
            totalThisMonth.setText("$0.00");
        }
        else    {
            NumberFormat currencyFormat = DecimalFormat.getCurrencyInstance(Locale.US);

            //last Fill Up

            lastFillUp.setText(currencyFormat.format(db.lastFillCost() / 100.0));

            //avg fill up
            averageFillUp.setText(currencyFormat.format(db.avgCost() / 100.0));

            //total this year
            totalThisYear.setText(currencyFormat.format(db.thisYearCost(cal.get(Calendar.YEAR))/100.0));

            //total this month
            totalThisMonth.setText(currencyFormat.format(db.thisMonthCost(cal.get(Calendar.MONTH))/100.0));
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_financial, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
