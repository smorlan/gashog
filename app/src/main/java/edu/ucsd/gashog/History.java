package edu.ucsd.gashog;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class History extends AppCompatActivity {

    private RelativeLayout _mainBtn;
    private RelativeLayout _finBtn;
    private RelativeLayout _addGasbtn;
    private RelativeLayout _settingsBtn;
    private ImageButton _mainBtn2;
    private ImageButton _finBtn2;
    private ImageButton _addGasbtn2;
    private ImageButton _settingsBtn2;

    private ArrayList<HistoryListItem> listItems;
    private Button allButton;
    private Button monthlyButton;
    private Button yearlyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        //hide action bar
        getSupportActionBar().hide();



        //create intents that we can navigate to from button clicks
        final Intent MainIntent = new Intent(this, MainActivity.class);
        final Intent AddGasIntent = new Intent(this, AddGas.class);
        final Intent FinancialsIntent = new Intent(this, Financial.class);
        final Intent SettingsIntent = new Intent(this, Settings.class);

        _mainBtn = (RelativeLayout) findViewById(R.id.home_history_button);
        _mainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainIntent);
            }
        });
        _mainBtn2 = (ImageButton) findViewById(R.id.home_history_button2);
        _mainBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainIntent);
            }
        });

        _addGasbtn = (RelativeLayout) findViewById(R.id.fill_gas_history_button);
        _addGasbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AddGasIntent);
            }
        });
        _addGasbtn2 = (ImageButton) findViewById(R.id.fill_gas_history_button2);
        _addGasbtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AddGasIntent);
            }
        });

        _finBtn = (RelativeLayout) findViewById(R.id.financial_history_button);
        _finBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FinancialsIntent);
            }
        });
        _finBtn2 = (ImageButton) findViewById(R.id.financial_history_button2);
        _finBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FinancialsIntent);
            }
        });

        _settingsBtn = (RelativeLayout) findViewById(R.id.settings_history_button);
        _settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingsIntent);
            }
        });
        _settingsBtn2 = (ImageButton) findViewById(R.id.settings_history_button2);
        _settingsBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingsIntent);
            }
        });

        allButton = (Button) findViewById(R.id.full_history);
        allButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateListItems("all");
            }
        });
        monthlyButton = (Button) findViewById(R.id.monthly);
        monthlyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateListItems("monthly");
            }
        });
        yearlyButton = (Button) findViewById(R.id.yearly);
        yearlyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateListItems("yearly");
            }
        });

        populateListItems("all");
    }

    private void populateListItems(String which) {
        /*ParseQuery<ParseObject> query = ParseQuery.getQuery("HistoryItem");
        query.fromLocalDatastore();*/

        ListView listView = (ListView) findViewById(R.id.list_history_listView);
        List<HistoryListItem> entries = new ArrayList<HistoryListItem>();
        listItems = new ArrayList<HistoryListItem>();
        HistoryDatabaseHelper db = new HistoryDatabaseHelper(this);
        if(which.equals("all")) {
            entries = db.getAllEntries();
        }

        if(which.equals("monthly")) {
            entries = db.getMonthlyEntries();
        }

        if(which.equals("yearly")) {
            entries = db.getYearlyEntries();
        }

        int i;
        for(i = entries.size()-1; i >= 0; i--) {
            listItems.add(entries.get(i));
        }

        //populate the listView
        HistoryListItemAdapter adapter = new HistoryListItemAdapter(this, listItems);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
